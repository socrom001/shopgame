/**
 * @author LiemNT
 */
package com.java12.controller;

import java.io.IOException;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.java12.model.Album;
import com.java12.model.Images;
import com.java12.service.AlbumService;
import com.java12.service.ContactService;
import com.java12.service.ImageService;

@Controller
public class AlbumController {
	@Autowired
	AlbumService albumService;
	@Autowired
	ImageService imgService;
	@Autowired
	ContactService contactService;

	@RequestMapping("/album")
	public String home(Model model) {
		List<Album> list = albumService.listAlbums();
		model.addAttribute("albums", list);
		return "album";
	}

	/* add album */
	@RequestMapping("/album/addAlbum")
	public String addAlbum(Model model, HttpSession session) {
		if (session.getAttribute("userId") == null || !session.getAttribute("userRole").equals("ADMIN") ) {
			return "401";
		}
		return "add_album";
	}
	@PostMapping("/addAlbum")
	public String addAlbum(@ModelAttribute("album") Album album, @RequestParam("myfile") MultipartFile multipartFile)
			throws IOException {
		String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
		album.setImage(multipartFile.getBytes());
		albumService.add(album);
		return "redirect:/album";
	}

	/* detail album */
	@RequestMapping("/album/{id}")
	public String addAlbum(Model model, @PathVariable("id") Long id) {
		List<Images> listImg = imgService.list(id);
		model.addAttribute("listImg", listImg);
		Album album = albumService.getAlbum(id);
		model.addAttribute("album", album);
		List<Album> listalbum = albumService.listAlbums();
		model.addAttribute("albums", listalbum);
		return "detail_album";
	}

	/* edit album */
	@RequestMapping("/albumEdit/{id}")
	public String editAlbum(Model model, @PathVariable("id") Long id, HttpSession session) {
		if (session.getAttribute("userId") == null || !session.getAttribute("userRole").equals("ADMIN") ) {
			return "401";
		}
		Album album = albumService.getAlbum(id);
		model.addAttribute("albumEdit", album);

		return "edit_album";
	}

	@PostMapping("/albumEdit/save")
	public String editAlbum(@ModelAttribute("albumEdit") Album album,
			@RequestParam("myfile") MultipartFile multipartFile) throws IOException {
		
		Album al = albumService.getAlbum(album.getId());
		if(multipartFile.isEmpty()==true) { 
			album.setImage(al.getImage());
		}
		else {
			String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
			album.setImage(multipartFile.getBytes());
		}
		albumService.save(album);
		return "redirect:/album";
	}

	/* add image album */
	@RequestMapping("/album/{id}/addImage")
	public String addImage(Model model, @PathVariable("id") Long id, HttpSession session) {
		if (session.getAttribute("userId") == null || !session.getAttribute("userRole").equals("ADMIN") ) {
			return "401";
		}
		Album album = albumService.getAlbum(id);
		model.addAttribute("album", album);
		return "add_image";
	}
	
	@PostMapping("/album/addImage")
	public String addImage(Model model,@ModelAttribute("image") Images image, @RequestParam("id_album") Long id_album, @RequestParam("myfile") MultipartFile multipartFile)
			throws IOException {
		
		String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
		image.setImage(multipartFile.getBytes());
		String id = id_album.toString();
		String link = "redirect:/album/" + id;
		imgService.add(image);
		return link;
	}

	/* delete album */
	@RequestMapping("/album/delete/{id}")
	public String deleteAlbum(@PathVariable Long id, Model model, HttpSession session) {
		if (session.getAttribute("userId") == null || !session.getAttribute("userRole").equals("ADMIN") ) {
			return "401";
		}
		albumService.deleteAlbum(id);
		return "redirect:/album";
	}

}
