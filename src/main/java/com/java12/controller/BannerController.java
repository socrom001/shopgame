package com.java12.controller;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import com.java12.model.Banner;

import com.java12.repositories.BannerRepository;
import com.java12.service.BannerService;

@Controller
@RequestMapping(value="/managebanner")
public class BannerController {
	private static String UPLOAD_DIR = "src/main/resources/static/assets/image/banner";
	@Autowired
	private BannerRepository bannerRepository;
	
	@Autowired
	private BannerService bannerService;
	@GetMapping(value="")
	public String managebannerpage(Model model) {
		List<Banner> listBanner =  bannerRepository.findAll();
		model.addAttribute("banners", listBanner);

		return "managebanner";
	}
	@GetMapping (value ="edit/{id}")
	public String editFormBanner(@PathVariable int id, Model model) {
		Banner banner = bannerRepository.getOne(id);
		model.addAttribute("banner", banner);
		return "managebannerEdit";
	}
	@PostMapping (value="/edit/{id}")
	public String updateBanner(HttpServletRequest request, //
            Model model,
            @ModelAttribute Banner banner,@ModelAttribute MultipartFile file,
            @PathVariable int id) {
		Banner bannerInRepo = bannerRepository.getOne(id);
		bannerInRepo.setAlt(banner.getAlt());
		try {
				bannerInRepo.setImage(file.getBytes());

		} catch (Exception e) {
			e.printStackTrace();
		}
		bannerInRepo.setPosition(banner.getPosition());
		bannerInRepo.setTitle(banner.getTitle());
		bannerRepository.save(bannerInRepo);
		return "redirect:/managebanner";
	}
	@GetMapping (value="/add")
	public String addFormBanner(Model model) {
		Banner banner = new Banner();
		model.addAttribute("banner", banner);
		return "managebannerAdd";
	}
	@PostMapping (value="/add")
	public String addFormBannerSubmit(HttpServletRequest request, //
            Model model,
            @ModelAttribute Banner banner,@ModelAttribute MultipartFile file) {
		try {
			banner.setImage(file.getBytes());
			bannerRepository.save(banner);
		} catch (Exception e) {
			e.printStackTrace();
		}
		bannerRepository.save(banner);
		return "redirect:/managebanner";
	}
	
	@GetMapping(value="deletefilebanner/{id}")
	public String deleteFileBanner(@PathVariable int id)
	{
		Banner banner = bannerRepository.getOne(id); 
		bannerRepository.delete(banner);
		return "redirect:/managebanner";
	}
	@GetMapping(value="2")
	public String home(Model model) {
		List<Banner> banners = bannerRepository.findAll();
//		System.out.println(banners);
		model.addAttribute("banners", banners);
		// System.out.println("123456");
		List<Banner> mid = new ArrayList<Banner>();
		
		for (Banner banner : banners) {
			if(banner.getPosition().equalsIgnoreCase("top-left")){
				model.addAttribute("topleft", banner);
				System.out.println(banner);
				break;
			}
		}
		for (Banner banner : banners) {
			if(banner.getPosition().equalsIgnoreCase("top-middle")) {
				mid.add(banner);
				System.out.println(banner);
			}
		}
		model.addAttribute("topmiddle",mid);
		for (Banner banner : banners) {
//			System.out.println(banner.getPosition().equalsIgnoreCase("top-right"));
			if(banner.getPosition().equalsIgnoreCase("top-right")) {
				model.addAttribute("topright", banner);
				System.out.println(banner);
				break;
			}
		}	
//		System.out.println("hello");
//		System.out.println(mid);
		return "home2";
	}
}
