package com.java12.controller;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.java12.model.Contact;
import com.java12.model.Product;
import com.java12.model.User;
import com.java12.repositories.BannerRepository;
import com.java12.service.BannerService;
import com.java12.service.ContactService;
import com.java12.service.ProductService;
import com.java12.service.UserService;

@Controller
public class HomePageController {
	@Autowired
	UserService userService;
	@Autowired
	PasswordEncoder passwordEncoder;
	@GetMapping("/greeting")
	public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
		model.addAttribute("name", name);
		
		return "greeting";
	}
	@Autowired 
	private ProductService productService;
	
	//insert  banner 9.21.2020
	@Autowired
	private BannerService bannerService;
	//end inseret banner 9.21.2020
	
	@GetMapping("/")
	public String home(Model model) {
		//set attribute for header
				List<Contact> contact = contactService.listAll();
				model.addAttribute("contactPhone", contact.get(0).getPhone());
				model.addAttribute("contactEmail", contact.get(0).getEmail());
				model.addAttribute("contactAddress", contact.get(0).getAddress());
				
				List<Product> hotPro =  productService.findNewProduct();
				System.out.println("hotttttttttttt");
				for(Product n:hotPro) {
					System.out.println("hot" +n.getId());
				}
				model.addAttribute("hotPro",hotPro);
				
				//insert  banner 9.21.2020
				model.addAttribute("topleft", bannerService.topleftBanner());
				model.addAttribute("topright", bannerService.toprightBanner());
				model.addAttribute("topmiddle", bannerService.topmidBanner());
				System.out.println("midder "+bannerService.topmidBanner());
				//end inseret banner 9.21.2020
				
		return "home";
	}
	
	//Mapping for Product Detail
	@GetMapping("/productDetail")
	public String productDetail(Model model) {
		
//		//set attribute for header
//		List<Contact> contact = contactService.listAll();
//		model.addAttribute("contactPhone", contact.get(0).getPhone());
//		model.addAttribute("contactEmail", contact.get(0).getEmail());
//		model.addAttribute("contactAddress", contact.get(0).getAddress());
		return "productDetail";
	}
	
	@GetMapping("/about")
	public String About(Model model) {
		//set attribute for header
		List<Contact> contact = contactService.listAll();
		model.addAttribute("contactPhone", contact.get(0).getPhone());
		model.addAttribute("contactEmail", contact.get(0).getEmail());
		model.addAttribute("contactAddress", contact.get(0).getAddress());
		return "about";
	}

	@PostMapping("/search")
	public String Search(Model model) {
		//set attribute for header
		List<Contact> contact = contactService.listAll();
		model.addAttribute("contactPhone", contact.get(0).getPhone());
		model.addAttribute("contactEmail", contact.get(0).getEmail());
		model.addAttribute("contactAddress", contact.get(0).getAddress());
		return "search";
	}

	//Kim's | Contact
	
	@Autowired
	private ContactService contactService; 
	
	@RequestMapping("/contact")
	public String contact(Model model) {
		List<Contact> listContacts = contactService.listAll();
		model.addAttribute("listContacts", listContacts);
		//set attribute for header
		List<Contact> contact = contactService.listAll();
		model.addAttribute("contactPhone", contact.get(0).getPhone());
		model.addAttribute("contactEmail", contact.get(0).getEmail());
		model.addAttribute("contactAddress", contact.get(0).getAddress());
		return "contact";
	}
	
	@RequestMapping(value = "/saveContact", method = RequestMethod.POST)
	public String saveContact(@ModelAttribute("contact") Contact contact) {
		contactService.save(contact);
		return "redirect:/contact";
	}
	
	@RequestMapping("/contactUpdate/{id}")
	public ModelAndView showContactUpdatePage(@PathVariable(name = "id") int id) {
		ModelAndView mav = new ModelAndView("contactUpdate");
		Contact contact = contactService.get(id);
		mav.addObject("contact", contact);
		return mav;
	}
	
	//Kim's | Contact | END
	
	
	//login test
	@PostMapping("/login")
	public String login(@RequestParam(name="loginUserEmail", required=false, defaultValue="") String userEamil,
			@RequestParam(name="loginUserPassword", required=false, defaultValue="") String password
			,Model model
			,HttpSession session) {
		if(userEamil.equals("") || password.equals("")) {
			model.addAttribute("messageError", "Wrong password or Email!!!");
			return "login";
		}
		
		try{
			Optional<User> user = userService.loadUserByEmail(userEamil);
			User userResult = user.get();
			if(passwordEncoder.matches(password, userResult.getPassword())) {
				session.setAttribute("userRole", userResult.getRole());
				session.setAttribute("userName", userResult.getName());
				session.setAttribute("userId", userResult.getId());
				List<Product> hotPro =  productService.findNewProduct();
				model.addAttribute("hotPro",hotPro);
				return "home";
			}
			else { 
				model.addAttribute("messageError", "Wrong password or Email!!!");
				return "login";
			}
		}catch(Exception e) {
			model.addAttribute("messageError", "Wrong password or Email!!!");
			return "login";
		}
		
	}
	
	@GetMapping("/logout")
	public String getlogin(@RequestParam(name="loginUserEmail", required=false, defaultValue="") String userEamil,
			@RequestParam(name="loginUserPassword", required=false, defaultValue="") String password
			,Model model,HttpSession session) {
			
			session.removeAttribute("userRole");
			session.removeAttribute("userName");
			session.removeAttribute("userId");
			
			List<Product> hotPro =  productService.findNewProduct();
			model.addAttribute("hotPro",hotPro);
			
			return "home";
		
	}
	@GetMapping("/login")
	public String getlogin(@RequestParam(name="loginUserEmail", required=false, defaultValue="") String userEamil,
			@RequestParam(name="loginUserPassword", required=false, defaultValue="") String password
			,Model model) {
			
			
			return "login";
		
	}
	
	@GetMapping("/layoutPage")
	private @ResponseBody Contact layout() {
		List<Contact> contact = contactService.listAll();
		return contact.get(0);
	}
	
	// Handling exception or something wrong with project
	
	@RequestMapping("*")
	public String fallbackMethod() {
		return "403";
	}
	
}

