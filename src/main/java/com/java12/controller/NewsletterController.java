package com.java12.controller;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.java12.model.Contact;
import com.java12.model.Newsletter;
import com.java12.service.ContactService;
import com.java12.service.NewsletterService;
import com.java12.service.UserService;


@Controller
public class NewsletterController {
	@Autowired
	NewsletterService newsletterService;
	
	@Autowired
	private ApplicationContext applicationContext;
	 
    public String getUrl() {
        try {
            String ip = InetAddress.getLocalHost().getHostAddress();
            int port = applicationContext.getBean(Environment.class).getProperty("server.port", Integer.class, 8080);
            return "http://" + ip + ":" + port;
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
		return "http://localhost";
    }
    
	@PostMapping("/newsletter")
	public ResponseEntity<HttpStatus> mailing(@RequestParam(value = "email") String email) {
		System.out.print(email);
		String TIME_SERVER = "time-a-b.nist.gov";   
		NTPUDPClient timeClient = new NTPUDPClient();
		InetAddress inetAddress;
		TimeInfo timeInfo;
		Boolean added = false;
		Newsletter newsletter = new Newsletter();
		try {
			inetAddress = InetAddress.getByName(TIME_SERVER);
			timeInfo = timeClient.getTime(inetAddress);
			long returnTime = timeInfo.getMessage().getTransmitTimeStamp().getTime();
			Date time = new Date(returnTime);
			DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			newsletter = new Newsletter(email,df.format(time));
			added = true;
			
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
			System.out.println("1_21");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("1_22");
		}
		System.out.println("1_2");
		if (added == false) {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
			LocalDateTime now = LocalDateTime.now();  
			newsletter = new Newsletter(email,dtf.format(now));
		}
		Optional <Newsletter> newsletter_ = newsletterService.getByEmail(email);
		if (newsletter_.isPresent()) {
			return (ResponseEntity<HttpStatus>) ResponseEntity.status(HttpStatus.CONFLICT);
		}
		newsletterService.save(newsletter);
		
		String hostUrl = getUrl();
		
		newsletterService.sendGreeting(hostUrl, email);
		
		System.out.println("1_");
		return ResponseEntity.ok(HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/send_newsletter")
	public ResponseEntity<HttpStatus> sendNewsletter(@RequestParam(value = "subject") String subject,
			@RequestParam(value = "content") String content, @RequestParam(value = "file") MultipartFile file) {
		System.out.println("send newsletter 1");
		System.out.println(file.getOriginalFilename());
		String bseDir = new File("").getAbsolutePath();
		System.out.println(bseDir);
		String filepath = bseDir +"\\src\\main\\resources\\static\\assets\\file\\" ;
		try {
			file.transferTo(new File(filepath +  file.getOriginalFilename()));
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			newsletterService.sendNews(subject,content,filepath, file.getOriginalFilename());
		} catch (MessagingException e) {
			ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR);
			e.printStackTrace();
		}
		return ResponseEntity.ok(HttpStatus.OK);
	}
	
	@RequestMapping("/newsletter_delete")
	public ResponseEntity<HttpStatus> delete(@RequestParam String email) {
		System.out.println("newsltr_del1");
		Optional <Newsletter> newsletter = newsletterService.getByEmail(email);
		if (newsletter.isPresent()) newsletterService.delete(newsletter.get().getId());
		return ResponseEntity.ok(HttpStatus.OK);
	}
	

	@GetMapping("/newslettercreate")
	public String createNewsletter(Model model) {
		return "newsletter";
	}
	
}

