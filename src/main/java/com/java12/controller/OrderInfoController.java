package com.java12.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.java12.model.OrderInfo;
import com.java12.service.OrderInfoService;

@Controller
public class OrderInfoController {
	@Autowired
	OrderInfoService orderService;
	
	@GetMapping("/user/order")
	public String showOrder(Model model, HttpSession session) {
		if(session.getAttribute("userId")!=null) {
			Long userId = (Long)session.getAttribute("userId");
			List<OrderInfo> listOrders = orderService.getUserOrders(userId);
			model.addAttribute("listOrders", listOrders);
			return "order";
		}
		else return "login";
	}
}
