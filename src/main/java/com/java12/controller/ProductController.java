package com.java12.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.java12.model.Product;
import com.java12.service.ContactService;
import com.java12.service.ProductService;

@Controller
public class ProductController {
	@Autowired
	private ProductService productService;
	@Autowired
	ContactService contactService;

	@GetMapping("/searchkodungden")
	public String greeting(@RequestParam(name = "keyword", required = false, defaultValue = "m") String keyword,
			Model model) {
		List<Product> searchProduct = productService.findByName(keyword);
		String productName = "";
		String productPrice = "";
		for (Product i : searchProduct) {
			productName += i.getName() + " ";
			productPrice += i.getPrice() + " ";
		}
		model.addAttribute("keyword", productName);
		model.addAttribute("cost", productPrice);
		model.addAttribute("lstsearch", searchProduct);
		return "search";
	}

	@RequestMapping("/product")
	public String productView(Model model) {
		List<Product> allProduct = productService.listAll();
		model.addAttribute("ProductAll", allProduct);
		return "product";
	}

	@RequestMapping("/productDetail/{id}")
	public String productDetail(Model model, @PathVariable("id") int id) {
		Product productDetail = productService.findProductById(id);
		List<Product> lstProductWithSameCate = productService.getProductByCategory(productDetail.getCategory());
		model.addAttribute("lstProductWithSameCate", lstProductWithSameCate);
		model.addAttribute("productDetail", productDetail);
		return "productDetail";
	}
	
	@RequestMapping("/product/category/{category}")
	public String productWithCatogory(Model model, @PathVariable("category") String category) {
		List<Product> allProduct = productService.getProductByCategory(category);
		model.addAttribute("ProductAll", allProduct);
		return "product";
	}

	/* Tien Loc */
	/* add product */
	@RequestMapping("/product/add")
	public String productAddView(Model model, HttpSession session) {

		// System.out.println("========================" +
		// session.getAttribute("userRole").equals("USER"));

		if (session.getAttribute("userId") == null || session.getAttribute("userRole").equals("USER")) {
			return "401";
		}

		Product product = new Product();
		model.addAttribute("product", product);
		model.addAttribute("listCategory", productService.listAllCategory());
		return "add_product";
	}

	/*LiemNT*/
	/* save product */
	@PostMapping("/product/add/save")
	public String editUser(@ModelAttribute("product") Product product,
			@RequestParam("imageProduct") MultipartFile multipartFile) throws IOException {
		product.setImage(multipartFile.getBytes());
		productService.addProduct(product);
		return "redirect:/product";
	}

	/* delete product */
	@RequestMapping("/product/delete/{id}")
	public String deleteProduct(@PathVariable Integer id, Model model) {
		productService.deleteProduct(id);
		return "redirect:/product";
	}

	/* edit product */
	@RequestMapping("/product/edit/{id}")
	public String editProduct(Model model, @PathVariable("id") int id) {
		Product product = productService.findProductById(id);
		model.addAttribute("product", product);
		return "edit_product";
	}
}