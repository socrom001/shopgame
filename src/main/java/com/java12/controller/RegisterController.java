package com.java12.controller;


import com.java12.model.User;
import com.java12.service.EmailService;
import com.java12.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Random;

@Controller
public class RegisterController {
    @Autowired
    UserService userService;

    @Autowired
    EmailService emailService;

    @Autowired
    PasswordEncoder passwordEncoder;

    private static String captchaCode = "";

    @PostMapping("/register")
    public String createNewUser(@ModelAttribute("user") User user, Model model, HttpServletRequest httpRequest) {
        //Check if email or phone numbet is existing, confirm password matching
        Optional<User> userByEmail = userService.loadUserByEmail(user.getEmail());
        Optional<User> userByPhoneNumber = userService.loadUserByPhoneNumber(user.getPhoneNumber());
        boolean isPassconfMatch = user.getPassword().equals(httpRequest.getParameter("passconf"));
        if (userByEmail.isPresent() || userByPhoneNumber.isPresent() ||
                !isPassconfMatch) {
            model.addAttribute("existingEmail", userByEmail.isPresent());
            model.addAttribute("existingPhoneNumber", userByPhoneNumber.isPresent());
            model.addAttribute("confirmPasswordNotMatch", !isPassconfMatch);

            return "register";
        }
        //Check PhoneNumber format
        if (!user.getPhoneNumber().matches("^[0-9]{10}$")) {
            model.addAttribute("phoneNumberErrorFormat", true);
            return "register";
        }
        //Check Captcha matching
        if (!httpRequest.getParameter("captcha").equals(captchaCode)) {
            model.addAttribute("captchaNotMatching", true);
            return "register";
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userService.saveUser(user);
        final String baseUrl = ServletUriComponentsBuilder.fromRequestUri(httpRequest)
                .replacePath(null)
                .build()
                .toUriString();
        new Thread(() -> {
            emailService.sendVerifyEmail(user.getEmail(), baseUrl, String.valueOf(user.getId()));
        }).start();
        return "redirect:/login";
    }

    @GetMapping("/register")
    public String registerPage(Model model) {
        model.addAttribute("user", new User());
        return "register";
    }

    @GetMapping("/verify-account/{userID}")
    public String confirmEmail(@PathVariable("userID") String id) {
        Optional<User> userOpt = userService.loadUserById(Integer.parseInt(id));
        User user = userOpt.orElseThrow(NoSuchElementException::new);
        user.setActivation(true);
        userService.saveUser(user);
        return "redirect:/login";
    }
    @GetMapping("/captcha")
    public void createCaptcha(HttpServletResponse response) throws ServletException, IOException {
        System.out.println("Start Captcha");
        response.setContentType("image/jpg");
        int iTotalChars = 6;
        int iHeight = 40;
        int iWidth = 150;
        Font fntStyle1 = new Font("Arial", Font.BOLD, 30);
        Random randChars = new Random();
        String sImageCode = (Long.toString(Math.abs(randChars.nextLong()), 36)).substring(0, iTotalChars);
        BufferedImage biImage = new BufferedImage(iWidth, iHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2dImage = (Graphics2D) biImage.getGraphics();
        int iCircle = 15;
        for (int i = 0; i < iCircle; i++) {
            g2dImage.setColor(new Color(randChars.nextInt(255), randChars.nextInt(255), randChars.nextInt(255)));
        }
        g2dImage.setFont(fntStyle1);
        for (int i = 0; i < iTotalChars; i++) {
            g2dImage.setColor(new Color(genRanNumInRange(100, 255), genRanNumInRange(100, 255), genRanNumInRange(100, 255)));
            if (i % 2 == 0) {
                g2dImage.drawString(sImageCode.substring(i, i + 1), 25 * i, 24);
            } else {
                g2dImage.drawString(sImageCode.substring(i, i + 1), 25 * i, 35);
            }
        }
        OutputStream osImage = response.getOutputStream();
        ImageIO.write(biImage, "jpg", osImage);
        g2dImage.dispose();
        captchaCode = sImageCode;
        System.out.println(sImageCode);
    }

    //Generate Random Numbers for define Captcha Color Text
    private int genRanNumInRange(int min, int max){
        return new Random().nextInt(max - min + 1) + min;
    }
}