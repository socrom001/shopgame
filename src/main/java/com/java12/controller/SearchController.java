package com.java12.controller;


import com.java12.model.Product;


import com.java12.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


import java.util.List;
import java.util.Optional;

@Controller
public class SearchController {

    @Autowired
    SearchService searchService;

    @RequestMapping("/search")
    String SearchView(@RequestParam(name="keyword", required = false, defaultValue = "a") String keyword,
    		@RequestParam(value = "page", required = false, defaultValue = "1") int page,
                      Model model){
        Page<Product> PsearchProduct = searchService.findName(keyword, page);
        long totalItems = PsearchProduct.getTotalElements();
        int totalPages = PsearchProduct.getTotalPages();

        List<Product> searchProduct = PsearchProduct.getContent();
        model.addAttribute("keyword",keyword);
        model.addAttribute("totalItems", totalItems);
        model.addAttribute("totalPages", totalPages);
        model.addAttribute("currentPage", page);
        model.addAttribute("searchProduct", searchProduct);
        model.addAttribute("currentPage", page);
        return "search";
    }
}
