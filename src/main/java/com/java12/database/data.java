/**
 * @author LiemNT
 */
package com.java12.database;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.io.IOUtils;
import org.apache.naming.java.javaURLContextFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.java12.model.Album;
import com.java12.model.Banner;
import com.java12.model.Contact;
import com.java12.model.Images;
import com.java12.model.News;
import com.java12.model.OrderInfo;
import com.java12.model.Product;
import com.java12.model.User;
import com.java12.model.Video;
import com.java12.repositories.AlbumRepository;
import com.java12.repositories.BannerRepository;
import com.java12.repositories.ContactRepository;
import com.java12.repositories.ImageRepository;
import com.java12.repositories.NewsRepository;
import com.java12.repositories.OrderInfoRepository;
import com.java12.repositories.ProductRepository;
import com.java12.repositories.UserRepository;
import com.java12.repositories.VideoRepository;

@Component
public class data {

	@Autowired
	AlbumRepository albumRepo;
	@Autowired
	ContactRepository contactRepo;
	@Autowired
	UserRepository userRepo;
	@Autowired
	NewsRepository newsRepo;
	@Autowired
	ImageRepository imgRepo;
	@Autowired
	PasswordEncoder passwordEncoder;
	@Autowired

	ProductRepository proRepo;
	
	@Autowired
	OrderInfoRepository orderRepo;
	
	@Autowired
	VideoRepository videoRepo;
	
	@Autowired
	BannerRepository bannerRepo;
	
	org.springframework.core.env.Environment env;

	// Prepare data for
	@EventListener
	public void appReady(ApplicationReadyEvent event) throws IOException {
		initDefaultAlbum();
		initDefaultUser();
		initDefaultNews();
		initDefaultContact();
		initDefaultImages();
		initDefaultProduct();
		initDefaultOrder();
		initDefaultVideo();
		initDefaultBanner();
	}

	public byte[] getImage(String image) throws IOException {
		File initialFile = new File("src/main/resources/static/assets/image/Album_Image/" + image);
		InputStream in = new FileInputStream(initialFile);
		return IOUtils.toByteArray(in);
	}

	private void initDefaultAlbum() {
		long millis = System.currentTimeMillis();
		java.sql.Date date = new java.sql.Date(millis);
		try {
			Album album1 = new Album("Album của Mòe", "4708cd711c24da5ad732ef3f05248b10.jpg", date, 132);
			Album album2 = new Album("Album của chó", "images (7).jfif", date, 32);
			Album album3 = new Album("Đôi bạn chó mèo", "tải xuống (7).jfif", date, 182);
			Album album4 = new Album("Chó mèo meme", "16_meitu_5_1.jpg", date, 321);
//			Album album5 = new Album("Du lịch Tây Bắc", "thumb_taybac-1478330403.jpg", date, 99);
//			Album album6 = new Album("Phụ kiện BK", "1.jpg", date, 1905);

			albumRepo.saveAndFlush(album1);
			albumRepo.saveAndFlush(album2);
			albumRepo.saveAndFlush(album3);
			albumRepo.saveAndFlush(album4);
//			albumRepo.saveAndFlush(album5);
//			albumRepo.saveAndFlush(album6);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	private void initDefaultImages() {
		
		try {
			Images img = new Images(1L, "927_chu_meo_hoan_hao_nhat_1.jpg", "image 1");
			Images img1 = new Images(1L, "4708cd711c24da5ad732ef3f05248b10.jpg", "image 2");
			Images img2 = new Images(1L, "cách-nuôi-mèo-từ-chuyên-gia-780x405.jpg", "image 3");
			Images img3 = new Images(2L, "images (6).jfif", "image 4");
			Images img4 = new Images(2L, "images (7).jfif", "image 5");
			Images img5 = new Images(2L, "images (8).jfif", "image 6");
			Images img6 = new Images(3L, "tải xuống (1).jfif", "image 7");
			Images img7 = new Images(3L, "tải xuống (2).jfif", "image 8");
			Images img8 = new Images(3L, "tải xuống (3).jfif", "image 9");
			Images img9 = new Images(3L, "tải xuống (4).jfif", "image 10");
			Images img10 = new Images(3L, "tải xuống (5).jfif", "image 11");
			Images img11 = new Images(4L, "images (10).jfif", "image 12");
			Images img12 = new Images(4L, "images (11).jfif", "image 13");
			Images img13 = new Images(4L, "images (12).jfif", "1.jpg");
			Images img14 = new Images(4L, "images (13).jfif", "2.jpg");
			Images img15 = new Images(4L, "images (4).jfif", "3.jpg");
			
			imgRepo.saveAndFlush(img);
			imgRepo.saveAndFlush(img1);
			imgRepo.saveAndFlush(img2);
			imgRepo.saveAndFlush(img3);
			imgRepo.saveAndFlush(img4);
			imgRepo.saveAndFlush(img5);
			imgRepo.saveAndFlush(img6);
			imgRepo.saveAndFlush(img7);
			imgRepo.saveAndFlush(img8);
			imgRepo.saveAndFlush(img9);
			imgRepo.saveAndFlush(img10);
			imgRepo.saveAndFlush(img11);
			imgRepo.saveAndFlush(img12);
			imgRepo.saveAndFlush(img13);
			imgRepo.saveAndFlush(img14);
			imgRepo.saveAndFlush(img15);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void initDefaultContact() {
		Contact contact0 = new Contact("02854145885", "Shopthucung@gmail.com",
				"trường đh Bách Khoa, khu phố 6, P. Linh Trung, Quận Thủ Đức, TP. HCM");
		contactRepo.saveAndFlush(contact0);
	}

	private void initDefaultUser() {

		User user1 = new User("admin1@gmail.com", "admin1", "0123456789", passwordEncoder.encode("123456"), "ADMIN");
		User user2 = new User("admin2@gmail.com", "admin2", "0123456799", passwordEncoder.encode("123456"), "ADMIN");
		User user3 = new User("user1@gmail.com", "user1", "0123456787", passwordEncoder.encode("123456"), "USER");
		User user4 = new User("user2@gmail.com", "user2", "0123456786", passwordEncoder.encode("123456"), "USER");
		User user5 = new User("user3@gmail.com", "user3", "0123456785", passwordEncoder.encode("123456"), "USER");
		User user6 = new User("user4@gmail.com", "user4", "0123456796", passwordEncoder.encode("123456"), "USER");
		User user7 = new User("shop1@gmail.com", "shop1", "0123456795", passwordEncoder.encode("123456"), "SHOP");
		User user8 = new User("shop2@gmail.com", "shop2", "0123456794", passwordEncoder.encode("123456"), "SHOP");
		userRepo.saveAndFlush(user1);
		userRepo.saveAndFlush(user2);
		userRepo.saveAndFlush(user3);
		userRepo.saveAndFlush(user4);
		userRepo.saveAndFlush(user5);
		userRepo.saveAndFlush(user6);
		userRepo.saveAndFlush(user7);
		userRepo.saveAndFlush(user8);
	}

	private void initDefaultNews() {
		long millis=System.currentTimeMillis();
		java.sql.Date date=new java.sql.Date(millis);
        News news1;
		try {
			news1 = new News("Nước diệt khuẩn, khử mùi ASFA – K ra mắt thị trường TP. HCM", "Tin tức y khoa - thiết bị y tế", date, 0, "Ngày 14/12, tại Tp.HCM, Công ty ASFA Việt Nam chính thức ra mắt sản phẩm ASFA –K, nước diệt khuẩn, khử mùi công nghệ cao của Nhật Bản.", "Ngày 14/12, tại Tp.HCM, Công ty ASFA Việt Nam chính thức ra mắt sản phẩm ASFA –K, nước diệt khuẩn, khử mùi công nghệ cao của Nhật Bản.\r\n" + 
					"\r\n" + 
					"Theo đó, ASFA – K mang đến thông điệp “Tái tạo môi trường vì sức khỏe cộng đồng”, có thể tiêu diệt 99,99% các loại vi trùng với tốc độ diệt khuẩn nhanh hơn 80 lần so với thông thường. Sản phẩm được chuyển giao công nghệ sản xuất và giám sát chất lượng trực tiếp từ Công ty WPAIA Co - Nhật Bản, đồng thời được kiểm nghiệm và chứng nhận an toàn theo tiêu chuẩn của Hoa Kỳ, HALAL bởi Bộ Y tế Nhật Bản và Bộ Y Tế Việt Nam."
					,geNewsImage("tin1.jpg"),"Asian Smart Trading – nhà phân phối của ASFA Việt Nam với các đối tác chiến lược");
			
			News news2 = new News("Dstore Global chung tay phòng dịch Covid -19", "Tin tức y khoa - thiết bị y tế", date, 1, "Ngày 23/3/2020, nhằm chung tay cùng Chính phủ trong công tác phòng chống dịch Covid-19 Kho hàng tổng Dstore đã tổ chức các hoạt động quyên góp, hưởng ứng", "Ngày 23/3/2020, nhằm chung tay cùng Chính phủ trong công tác phòng chống dịch Covid-19 Kho hàng tổng Dstore đã tổ chức các hoạt động quyên góp,  hưởng ứng lời kêu gọi “tương thân tương ái”, “chống dịch như chống giặc” của thủ tướng chính phủ trên phạm vi toàn quốc.\r\n" + 
					"\r\n" + 
					"Là một doanh nghiệp Tổng kho phân phối đa ngành hàng trong nước và quốc tế phân phối nhiều sản phẩm mỹ phẩm, thực phẩm chức năng, nước hoa, thời trang… chất lượng cho người tiêu dùng cả nước. Bên cạnh cung cấp những sản phẩm thương hiệu, an toàn và chất lượng, Kho hàng tổng Dstore luôn nỗ lực,trách nhiệm với cộng đồng trong các hoạt động đóng góp xã hội ở các lĩnh vực giáo dục, các trẻ em mồ côi…và nay là chung tay khuyên góp để phòng chống dịch bệnh Covid-19."
					,geNewsImage("tin2.jpg"),"Ban Lãnh Đạo Dgroup Holdings –Chủ Tịch Hồ Huỳnh Duy trực tiếp chỉ đạo chiến dịch “Chống dịch như chống giặc” cùng cán bộ nhân viên");
	        
			News news3 = new News("#ỞNhà #StayHome #WorkFromHome #WorkAtHome", "Tin tức y khoa - thiết bị y tế", date, 10, "#ỞNhà #StayHome #WorkFromHome #WorkAtHome", "Hưởng ứng lời kêu gọi của Chính Phủ và chính quyền Hà Nội, TP HCM, từ nay cả nhà #DSTORE, #DGH chúng tôi sẽ kiểm soát nghiêm ngặt tại chỗ làm, hạn chế tối đa nhân sự, song song làm việc tại nhà, WORK FROM HOME. Đây là thời khắc mọi người sẽ cùng chung tay để kiểm soát và đẩy lùi COVID -19.\r\n" + 
					"\r\n" + 
					"Chia sẻ mọi người một số đúc kết kinh nghiệm sưu tập được để làm việc hiệu quả tại nhà, WORK FROM HOME:",
					geNewsImage("tin3.jpg"),"");
	        News news4 = new News("Hội nghị sơ kết triển khai Đề án Bệnh viện vệ tinh chuyên ngành ung bướu", "Tin tức y khoa - thiết bị y tế", date, 11, "Ngày 06/07/2019, Bệnh viện K phối hợp với Sở Y tế tỉnh Bắc Ninh tổ chức Hội nghị sơ kết triển khai Đề án Bệnh viện vệ tinh chuyên ngành ung bướu, Dự án Norred,", "Ngày 06/07/2019, Bệnh viện K phối hợp với Sở Y tế tỉnh Bắc Ninh tổ chức Hội nghị sơ kết triển khai Đề án Bệnh viện vệ tinh chuyên ngành ung bướu, Dự án Norred, công tác chỉ đạo tuyến giai đoạn 2013 – 2018 và triển khai kế hoạch giai đoạn 2019 – 2020. Tham dự chương trình có GS.TS Nguyễn Viết Tiến, Thứ trưởng thường trực Bộ Y tế; các đồng chí lãnh đạo đại diện Vụ, Cục Bộ Y tế; lãnh đạo UBND tỉnh Bắc Ninh; GS.TS Trần Văn Thuấn, Giám đốc Bệnh viện K, các đồng chí trong Ban lãnh đạo bệnh viện K; bệnh viện vệ tinh, các bệnh viện tham gia dự án Norred, chỉ đạo tuyến và đông đảo cơ quan thông tấn, báo chí."
	        		,geNewsImage("tin4.jpg"),"Đoàn chủ tọa Hội nghị");
	    
	        newsRepo.saveAndFlush(news1);
	        newsRepo.saveAndFlush(news2);
	        newsRepo.saveAndFlush(news3);
	        newsRepo.saveAndFlush(news4);  
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
         
	}
	public byte[] geNewsImage(String image) throws IOException {
		File initialFile = new File("src/main/resources/static/assets/image/news_images/" + image);
		InputStream in = new FileInputStream(initialFile);
		return IOUtils.toByteArray(in);	
		
	}
	
	public byte[] OrderImage(String image) throws IOException {
		File initialFile = new File("src/main/resources/static/assets/image/product_images/" + image);
		InputStream in = new FileInputStream(initialFile);
		return IOUtils.toByteArray(in);	
		
	}
	
	private void initDefaultProduct() throws IOException {
		System.out.println("----------------------------------------------");
		Product product1 = new Product("Chuồng đôi cho chó mèo","chuong/chuong-cho-meo-lap-ghep-2-ngan-bang-sat-iris-pcs1400-400x400.jpg","10000000","Mua một cái ổ nằm cho thú cưng để giúp chúng cảm thấy bớt sợ hãi hơn. Thú cưng sau khi đến một môi trường mới xa lạ thường sẽ sinh ra cảm giác sợ hãi, cô đơn.",7,"Chuồng cho thú cưng");
		Product product2 = new Product("Chuồng 2 tầng đôi cho mèo","chuong/chuong-go-cho-cho-meo-2-tang-RICHELL-rc09-400x400.jpg","10000","Đây sản phẩm chuồng cho chó mèo",7,"Chuồng cho thú cưng");
		Product product3 = new Product("Chuồng 2 tầng siêu rộng","chuong/chuong-go-cho-cho-meo-2-tang-RICHELL-rc19-400x400.jpg","20000","Đây sản phẩm chuồng cho chó mèo",2,"Chuồng cho thú cưng");
		Product product4 = new Product("Chuong đôi cho chó","chuong/chuong-go-cho-cho-meo-3-tang-RICHELL-rc20-400x400.jpg","20000","Đây sản phẩm chuồng cho chó mèo",2,"Chuồng cho thú cưng");
		Product product5 = new Product("Chuồng cho mèo màu hồng","chuong/chuong-quay-cho-cho-bang-nhua-nan-mong-aupet-161-400x400.jpg","20000","Đây sản phẩm chuồng cho chó mèo",2,"Chuồng cho thú cưng");
		Product product6 = new Product("Chuồng sắt cho mèo sàn nhựa có mái che","chuong/chuong-sat-cho-cho-meo-san-nhua-co-mai-aupet-0933-size-m-400x400.jpg","50000","Đây sản phẩm chuồng cho chó mèo",1,"Chuồng cho thú cưng");
		Product product7 = new Product("Nhà cây cây cho mèo ","chuong/nha-cay-cho-meo-cat-tree-bobo-f3031-510x510-510x510.jpg","50000","Đây sản phẩm chuồng cho chó mèo",0,"Chuồng cho thú cưng");
		Product product8 = new Product("Nhà cây cây cho mèo nhiều tầng","chuong/nha-cay-cho-meo-cat-tree-qq80167-510x510.jpg","1000","Đây sản phẩm chuồng cho chó mèo",100,"Chuồng cho thú cưng");
		Product product10 = new Product("Chuông cho chó mèo nhiều màu sắc","dochoi/chuong-cho-cho-meo-nhieu-sac-mau-510x510.jpg","500000","Đây sản phẩm đồ chơi cho chó mèo",3,"Đồ chơi cho thú cưng");
		Product product9 = new Product("Dây đeo chó mèo đủ màu sắc","dochoi/co-du-7-mau-theu-2-510x510.jpg","50000","Đây sản phẩm đồ chơi cho chó mèo",0,"Đồ chơi cho thú cưng");
		Product product11 = new Product("Dây dắt cho mèo họa tiết chấm bi","dochoi/day-dat-hoa-tiet-cham-bi-510x510.jpg","500000","Đây sản phẩm đồ chơi cho chó mèo",3,"Đồ chơi cho thú cưng");
		Product product12 = new Product("Mũ cho mèo ambaby pet","thoitrang/mu-cho-cho-meo-ambaby-pet-1jxs071-1-250x300.jpg","100000","Đây sản phẩm thời trang cho chó mèo",3,"Thời trang cho thú cưng");
		Product product13 = new Product("Nơ cho mèo caro","thoitrang/no-cho-cho-meo-250x300.jpg","50000","Đây sản phẩm thời trang cho chó mèo",3,"Thời trang cho thú cưng");
		Product product14 = new Product("Quần áo cho mèo ambaby pet","thoitrang/quan-ao-cho-cho-meo-ambaby-pet-2jxf106-2-250x300.jpg","50000","Đây sản phẩm thời trang cho chó mèo",3,"Thời trang cho thú cưng");
		Product product15 = new Product("Quần áo cho chó mèo","thoitrang/quan-ao-cho-cho-meo-ambaby-pet-2jxf112-250x300.jpg","50000","Đây sản phẩm thời trang cho chó mèo",3,"Thời trang cho thú cưng");
		Product product16 = new Product("Sữa bột esbilac chó con","thucan/sua_bot_esbilac_cho_con-510x510.jpg","50000","Đây sản phẩm thức ăn cho chó mèo",3,"Thức ăn cho thú cưng");
		Product product17 = new Product("Sữa bột cho chó mèo petlac","thucan/sua-bot-cho-cho-meo-petlac-510x510.jpg","50000","Đây sản phẩm thức ăn cho chó mèo",3,"Thức ăn cho thú cưng");
		Product product18 = new Product("sushi bites cho chó","thucan/sushi-bites-cho-cho-510x510.png","50000","Đây sản phẩm thức ăn cho chó mèo",3,"Thức ăn cho thú cưng");
		Product product19 = new Product("Thuốc nhỏ tai dexoryl","thuoc/thuoc-nho-tai-dexoryl-250x300.jpg","50000","Đây sản phẩm thuốc cho chó mèo",3,"Thuốc cho thú cưng");
		proRepo.saveAndFlush(product1);
		proRepo.saveAndFlush(product2);
		proRepo.saveAndFlush(product3);
		proRepo.saveAndFlush(product4);
		proRepo.saveAndFlush(product5);
		proRepo.saveAndFlush(product6);
		proRepo.saveAndFlush(product7);
		proRepo.saveAndFlush(product8);
		proRepo.saveAndFlush(product9);
		proRepo.saveAndFlush(product10);
		proRepo.saveAndFlush(product11);
		proRepo.saveAndFlush(product12);
		proRepo.saveAndFlush(product13);
		proRepo.saveAndFlush(product14);
		proRepo.saveAndFlush(product15);
		proRepo.saveAndFlush(product16);
		proRepo.saveAndFlush(product17);
		proRepo.saveAndFlush(product18);
		proRepo.saveAndFlush(product19);
	}
	
	private void initDefaultOrder() throws IOException {
		long millis=System.currentTimeMillis();
		java.sql.Date date = new java.sql.Date(millis);
		OrderInfo order1 = new OrderInfo(date, date, OrderImage("thuoc/thuoc-nho-tai-dexoryl-250x300.jpg") , "Cheems", "Know Your Meme",
				86000, 1, 22000, "Chưa giao", 10000, "Netherworld, Phường 10000, Thành phố Vĩnh Long, Vĩnh Long, Việt Nam",new Long(3));
		OrderInfo order2 = new OrderInfo(date, date, OrderImage("thuoc/thuoc-nho-tai-dexoryl-250x300.jpg"), "Cheems", "Know Your Meme",
				86000, 1, 22000, "Chưa giao", 10000, "Netherworld, Phường 10000, Thành phố Vĩnh Long, Vĩnh Long, Việt Nam",new Long(3));
		OrderInfo order3 = new OrderInfo(date, date,OrderImage("thuoc/thuoc-nho-tai-dexoryl-250x300.jpg"), "Cheems", "Know Your Meme",
				86000, 1, 22000, "Chưa giao", 10000, "Netherworld, Phường 10000, Thành phố Vĩnh Long, Vĩnh Long, Việt Nam",new Long(5));
		orderRepo.saveAndFlush(order1);
		orderRepo.saveAndFlush(order2);
		orderRepo.saveAndFlush(order3);
	}	
	
	private void initDefaultVideo() {

			
			long millis=System.currentTimeMillis();
			java.sql.Date date=new java.sql.Date(millis);
		
			Video video0 = new Video(
					"Tik Tok Chó phốc sóc mini Funny and Cute Pomeranian Videos", 
					"https://img.youtube.com/vi/lMyNcEB2iBw/mqdefault.jpg",
					"https://www.youtube.com/embed/lMyNcEB2iBw", 
					date, 
					(long) 1000,  
					"Tik Tok Chó phốc sóc mini Funny and Cute Pomeranian Videos");
			videoRepo.saveAndFlush(video0);
			
			
			Video video1 = new Video(
					"Thư giãn Cùng Boss Cưng Đáng Yêu Cute Không Chịu Nổi P(13) 🐱🐶 Funny Dog & Cat Videos Compilation", 
					"https://img.youtube.com/vi/m-z5tQZAn_U/mqdefault.jpg",
					"https://www.youtube.com/embed/m-z5tQZAn_U", 
					date, 
					(long) 1000,  
					"Thư giãn Cùng Boss Cưng Đáng Yêu Cute Không Chịu Nổi P(13) 🐱🐶 Funny Dog & Cat Videos Compilation\r\n" + 
					"Không chỉ Bệnh viện Bạch Mai, theo một số nguồn tin mà Phóng viên Chào buổi tối nắm được, một bệnh viện khác tại Hà Nội cũng đặt thiết bị y tế robot Rosa do Công ty BMS cung cấp đã bị nâng khống lên 39 tỷ, gấp 4 lần giá nhập theo khai báo hải quan.");	
			Video video2 = new Video(
					"Cười lăn lóc với những tình huống hài hước lầy lội của động vật | Tik Tok động vật #2", 
					"https://img.youtube.com/vi/5t_Dzj4rfNo/mqdefault.jpg",
					"https://www.youtube.com/embed/5t_Dzj4rfNo", 
					date, 
					(long) 1000,  
					"Cười lăn lóc với những tình huống hài hước lầy lội của động vật | Tik Tok động vật #2");
			Video video3 = new Video(
					"Chú vịt Bỏ trốn", 
					"https://img.youtube.com/vi/U-C0I-Ks14M/mqdefault.jpg",
					"https://www.youtube.com/embed/U-C0I-Ks14M", 
					date, 
					(long) 1000,  
					"\"Vịt con của Hoàng tử Michael trở về nhà vào mùa hè. Michael để ý rằng Gabe đang gặp vấn đề.");	
			Video video4 = new Video(
					"WORLD'S LARGEST HAMSTER MAZE -Obstacle course!", 
					"https://img.youtube.com/vi/hl4cmSvms98/mqdefault.jpg",
					"https://www.youtube.com/embed/hl4cmSvms98", 
					date, 
					(long) 1000,  
					"Can hamster go through the world's largest maze with obstacles?");	
			Video video5 = new Video(
					"Fun Pet Care Game - Little Kitten Adventures - Play Costume Dress-Up Party Mini Games For Children", 
					"https://img.youtube.com/vi/BKpV8xqz_9E/mqdefault.jpg",
					"https://www.youtube.com/embed/BKpV8xqz_9E", 
					date, 
					(long) 1000,  
					"Fun Pet Care Game - Little Kitten Adventures - Play Costume Dress-Up Party Mini Games For Children");
			Video video6 = new Video(
					" Thư giãn cùng boss cưng đáng yêu", 
					"https://img.youtube.com/vi/BKpV8xqz_9E/mqdefault.jpg",
					"https://www.youtube.com/embed/BKpV8xqz_9E", 
					date, 
					(long) 1000,  
					"Hiện tại vì đang có một chút vất vả trong công việc nên chưa thế chuyên tâm làm nhiều clip boss cưng cute cho các bạn được, mạn phép nhận donate:");
			videoRepo.saveAndFlush(video0);
			videoRepo.saveAndFlush(video1);
			videoRepo.saveAndFlush(video2);
			videoRepo.saveAndFlush(video3);
			videoRepo.saveAndFlush(video4);
			videoRepo.saveAndFlush(video5);
			videoRepo.saveAndFlush(video6);
				
		}
	private void initDefaultBanner() {
		try {
			Banner banner = new Banner("title1","google.com","image1","top-right","src/main/resources/static/assets/image/banner/banner3.png");
			Banner banner2 = new Banner("title2","google.com","image1","top-middle","src/main/resources/static/assets/image/banner/banner1.jpg");
			Banner banner3 = new Banner("title3","google.com","image1","top-left","src/main/resources/static/assets/image/banner/banner3.jpg");
			Banner banner4 = new Banner("title2","google.com","image1","top-middle","src/main/resources/static/assets/image/banner/bannerslide1.jpg");
			Banner banner5 = new Banner("title2","google.com","image1","top-middle","src/main/resources/static/assets/image/banner/bannerslide2.jpg");
			Banner banner6 = new Banner("title2","google.com","image1","top-middle","src/main/resources/static/assets/image/banner/bannerslide3.jpg");
			bannerRepo.saveAndFlush(banner);
			bannerRepo.saveAndFlush(banner2);
			bannerRepo.saveAndFlush(banner3);
			bannerRepo.saveAndFlush(banner4);
			bannerRepo.saveAndFlush(banner5);
			bannerRepo.saveAndFlush(banner6);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}