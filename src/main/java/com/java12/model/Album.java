/*LiemNT*/
package com.java12.model;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.tomcat.util.codec.binary.Base64;
import org.imgscalr.Scalr;

@Entity
public class Album {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String name;
	private Date date;
	private Integer view;
	@Lob
	private byte[] image;

	public Album() {
	}
	public Album(Long id, Date date, Integer view,String name, String imagePath) throws IOException {
		this.id = id;
		this.name = name;
		this.image = this.resizeImage(this.getAlbumImage(imagePath));
		if (date == null) {
			long millis = System.currentTimeMillis();
			java.sql.Date now = new java.sql.Date(millis);
			this.date = now;
		} else {
			this.date = date;
		}
		if (view == null) {
			this.view = 0;
		} else {
			this.view = view;
		}
	}
	public Album(Long id,String name, String imagePath) throws IOException  {
		this.id = id;
		this.name = name;
		this.image = this.resizeImage(this.getAlbumImage(imagePath));
		
	}
	public Album(Long id,String name)  {
		this.id = id;
		this.name = name;
	}
	public Album(String name, String imagePath, Date date, Integer view) throws IOException {
		this.name = name;
		this.image = this.resizeImage(this.getAlbumImage(imagePath));
		if (date == null) {
			long millis = System.currentTimeMillis();
			java.sql.Date now = new java.sql.Date(millis);
			this.date = now;
		} else {
			this.date = date;
		}
		if (view == null) {
			this.view = 0;
		} else {
			this.view = view;
		}
	}

	public Album(String name, byte[] image) throws IOException {
		this.name = name;
		if (image == null) {
			File initialFile = new File("src/main/resources/static/assets/image/Album_Image/no-images.jpg");
			InputStream in = new FileInputStream(initialFile);
			this.image = IOUtils.toByteArray(in);
		} else {

			this.image = image;
		}

	}

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte[] getImage() {
		return image;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getView() {
		return view;
	}

	public void setView(Integer view) {
		this.view = view;
	}

	public String generateBase64Image() {
		return Base64.encodeBase64String(this.getImage());
	}

	public byte[] getAlbumImage(String image) throws IOException {
		File initialFile = new File("src/main/resources/static/assets/image/Album_Image/" + image);
		InputStream in = new FileInputStream(initialFile);
		return IOUtils.toByteArray(in);
	}

	public byte[] resizeImage(byte[] image) throws IOException {
		if (image.length < 1048576) {
			return image;
		}
		InputStream targetStream = new ByteArrayInputStream(image);
		BufferedImage originalImage = ImageIO.read(targetStream);
		originalImage = Scalr.resize(originalImage, Scalr.Method.QUALITY, Scalr.Mode.FIT_EXACT, 512, 512);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(originalImage, "png", baos);
		baos.flush();
		return baos.toByteArray();
	}

	public void setImage(byte[] image) throws IOException {
		long millis = System.currentTimeMillis();
		java.sql.Date now = new java.sql.Date(millis);
		if (image.length == 0) {
			File initialFile = new File("src/main/resources/static/assets/image/news_images/no-images.jpg");
			InputStream in = new FileInputStream(initialFile);
			this.image = this.resizeImage(IOUtils.toByteArray(in));
		} else {

			this.image = this.resizeImage(image);
		}
		this.date = now;
		this.view = 0;
	}

}
