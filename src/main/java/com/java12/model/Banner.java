package com.java12.model;

import java.io.File;
import java.nio.file.Files;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.apache.tomcat.util.codec.binary.Base64;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@Entity
@Table(name="banner")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class Banner {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	
	private Integer id ;
	
	@Column(name="title")
	private String title;
	
	@Column(name="url")
	private String url;
	
	@Column(name="alt")
	private String alt;
	
	@Column(name="position")
	private String position;
	@Lob
	private byte[] image;
	public Banner(String title, String url, String alt, String position, String image) {
		this.title = title;
		this.url = url;
		this.alt = alt;
		this.position = position;
		
		File file = new File(image);
		try {			
			this.image = Files.readAllBytes(file.toPath());
		} catch (Exception e) {
			e.getStackTrace();
		}
		
	}
	
	public Banner() {
		
	}
	
	public String generateBase64Image() {
		return Base64.encodeBase64String(this.getImage());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getAlt() {
		return alt;
	}

	public void setAlt(String alt) {
		this.alt = alt;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return "Banner [id=" + id + ", title=" + title + ", url=" + url + ", alt=" + alt + ", position=" + position
				+ "]";
	}
	
}
