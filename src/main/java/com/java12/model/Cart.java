package com.java12.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class Cart {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	private Long userId;
	private int quantityP;
	private int totalPrice;
	private int productId;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public Cart() {
	
	}
	
	public Cart(int quantityP, int totalPrice, int productId) {
		super();
		this.quantityP = quantityP;
		this.totalPrice = totalPrice;
		this.productId = productId;
	}
	
	public int getQuantityP() {
		return quantityP;
	}
	
	public void setQuantityP(int quantityP) {
		this.quantityP = quantityP;
	}
	
	public int getTotalPrice() {
		return totalPrice;
	}
	
	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	public int getProduct() {
		return productId;
	}
	
	public void setProduct(int productId) {
		this.productId = productId;
	}
	
	public Cart(int quantityP, int totalPrice, int productId, Long userId) {
		super();
		this.quantityP = quantityP;
		this.totalPrice = totalPrice;
		this.productId = productId;
		this.userId= userId;
	}
}