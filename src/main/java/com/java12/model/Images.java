package com.java12.model;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.tomcat.util.codec.binary.Base64;
import org.imgscalr.Scalr;

@Entity
public class Images {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long id_album;
    private String name;
    @Lob
    private byte[] img;

    public Images() {}
    
    public Images(Long id_album, byte[] img) {
    	this.id_album = id_album;
    	this.img = img;
    }
   

    public Images(Long id_album,String imagePath, String name) throws IOException {
		this.name = name;
		this.id_album = id_album;
		this.img = this.resizeImage(this.getImage(imagePath));
		
	}
    public Images(Long id,Long id_album, String name,String imagePath) throws IOException {
  		this.id = id;
    	this.name = name;
  		this.id_album = id_album;
  		this.img = this.resizeImage(this.getImage(imagePath));
  		
  	}

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId_album() {
		return id_album;
	}

	public void setId_album(Long id_album) {
		this.id_album = id_album;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte[] getImg() {
		return img;
	}

	public void setImg(byte[] img) {
		this.img = img;
	}

	public String generateBase64Image()
	{
	    return Base64.encodeBase64String(this.getImg());
	}
	public byte[] getImage(String image) throws IOException {
		File initialFile = new File("src/main/resources/static/assets/image/Album_Image/" + image);
		InputStream in = new FileInputStream(initialFile);
		return IOUtils.toByteArray(in);
	}
	public byte[] resizeImage(byte[] image) throws IOException {
		if (image.length < 1048576) {
			return image;
		}
		InputStream targetStream = new ByteArrayInputStream(image);
		BufferedImage originalImage = ImageIO.read(targetStream);
		originalImage = Scalr.resize(originalImage, Scalr.Method.QUALITY, Scalr.Mode.FIT_EXACT, 512, 512);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(originalImage, "png", baos);
		baos.flush();
		return baos.toByteArray();
	}
	public void setImage(byte[] image) throws IOException {
		long millis = System.currentTimeMillis();
		java.sql.Date now = new java.sql.Date(millis);
		if (image.length == 0) {
			File initialFile = new File("src/main/resources/static/assets/image/news_images/no-images.jpg");
			InputStream in = new FileInputStream(initialFile);
			this.img = this.resizeImage(IOUtils.toByteArray(in));
			System.out.println("img src null:");
		} else {

			this.img = this.resizeImage(image);
		}
		
	}
    
}
