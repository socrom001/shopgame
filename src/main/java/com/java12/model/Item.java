package com.java12.model;

public class Item {
	Long id;
	Product product;
	int quantityInCart;
	
	public Long getId() {
		return id;
	}
	
//	public void deleteItem(int id) {
//		Item.remove();
//	}
	
	public void setId(long id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}
	
	public void setProduct(Product product) {
		this.product = product;
	}
	
	public int getQuantityInCart() {
		return quantityInCart;
	}
	
	public void setQuantityInCart(int quantityInCart) {
		this.quantityInCart = quantityInCart;
	}
	public Item(Long id, Product product, int quantityInCart) {
		super();
		this.id=id;
		this.product = product;
		this.quantityInCart = quantityInCart;
	}
	
	public Item() {
		
	}
}