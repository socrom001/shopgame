package com.java12.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;

import javax.management.loading.PrivateClassLoader;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.apache.commons.io.IOUtils;
import org.apache.tomcat.util.codec.binary.Base64;

@Entity
@Table(name="news")
public class News {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private int id;
	
	private String title;
	private String category;
	private Date created;
	private int view;
	
	@Lob
	private String decription;
	
	/* Quang Dai - Content News */
	@Lob
	private String content;
	
    @Lob
    private byte[] image;
	private String imageTitle;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public int getView() {
		return view;
	}
	public void setView(int view) {
		this.view = view;
	}
	public String getDecription() {
		return decription;
	}
	public void setDecription(String decription) {
		this.decription = decription;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public byte[] getImage() {
		return image;
	}
	
	public String getImageTitle() {
		return imageTitle;
	}
	public void setImageTitle(String imageTitle) {
		this.imageTitle = imageTitle;
	}
	public News() {
		super();
	}
	public News(String title, String category, Date created, int view, String decription, String content, byte[] image,
			String imageTitle) {
		super();
		this.title = title;
		this.category = category;
		this.created = created;
		this.view = view;
		this.decription = decription;
		this.content = content;
		this.image = image;
		this.imageTitle = imageTitle;
	}
	public News(int id, String title, String category, Date created, int view, String decription, String content,
			byte[] image, String imageTitle) {
		super();
		this.id = id;
		this.title = title;
		this.category = category;
		this.created = created;
		this.view = view;
		this.decription = decription;
		this.content = content;
		this.image = image;
		this.imageTitle = imageTitle;
	}
	
	public News(String content) {
		this.content = content;
	}

	public String generateBase64Image()
	{
	    return Base64.encodeBase64String(this.getImage());
	}

	public void setImage(byte[] image) throws IOException {
		if (image.length == 0) {
			File initialFile = new File("src/main/resources/static/assets/image/news_images/no-images.jpg");
			InputStream in = new FileInputStream(initialFile);
			image = IOUtils.toByteArray(in);
		} 
		this.view = 0;
		this.image = image;
	}
	
	
	
	
	

}