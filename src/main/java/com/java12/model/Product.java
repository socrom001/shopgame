	package com.java12.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.persistence.*;

import org.apache.commons.io.IOUtils;
import org.apache.tomcat.util.codec.binary.Base64;

@Entity
@Table(name="product")
public class Product {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column
    private String name;
    @Lob
    private byte[] image;
    private String price;
    private String description;
	private int quantity;
	private String category;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public byte[] getImage() {
		return image;
	}
    
	public void setImage(byte[] image) {
		this.image = image;
	}

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
    
	public int getQuantity() {
		return quantity;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
    public Product() {
    	super();
    }

    public Product(String name,byte[] image ,String price, String description, int quantity, String category) {
        this.name = name;
        this.image = image;
        this.price = price;
        this.setDescription(description);
		this.quantity = quantity;
		this.category = category;
    }
    
    public Product(String name,String image ,String price, String description, int quantity, String category) throws IOException {
        
    	this.name = name;
        this.image = this.getProductImage(image);
        this.price = price;
        this.setDescription(description);
		this.quantity = quantity;
		this.category = category;
    }

    public String generateBase64Image() {
		return Base64.encodeBase64String(this.getImage());
	}

	public byte[] getProductImage(String image) throws IOException {
		
		File initialFile = new File("src/main/resources/static/assets/image/product_images/" + image);
		InputStream in = new FileInputStream(initialFile);
		return IOUtils.toByteArray(in);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
