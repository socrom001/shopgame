package com.java12.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.java12.model.Contact;

public interface ContactRepository extends JpaRepository<Contact, Long>{

}
