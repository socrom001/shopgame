package com.java12.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.java12.model.News;


@Repository
public interface NewsRepository extends JpaRepository<News, Integer> {
    News findById(int id);
    List<News> findAll();
    List<News> findByCategory(String category);
    
    @Query(value = "SELECT * FROM news ORDER BY created DESC", nativeQuery = true)
    List<News> findOrderByCreated();
    
    @Query(value = "SELECT * FROM news ORDER BY created DESC", nativeQuery = true)
    List<News> findOrderByView();
	 
	/* Quang Dai - List all Category */
    @Query(value = "SELECT DISTINCT category FROM news", nativeQuery = true)
	List<String> listAllCategory();
}
