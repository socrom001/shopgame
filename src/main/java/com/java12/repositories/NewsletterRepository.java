package com.java12.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.java12.model.Newsletter;

public interface NewsletterRepository extends JpaRepository<Newsletter, Long>{
	Optional<Newsletter> findByEmail(String email);
}
