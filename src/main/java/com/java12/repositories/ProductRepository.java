package com.java12.repositories;

import com.java12.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
    Product findById(int id);
    List<Product> findByNameContaining(String name);
    
    @Query(value = "SELECT * FROM product ORDER BY id DESC", nativeQuery = true)
    List<Product> findOrderById();
    
    List<Product> findByCategory(String category);
    
    @Query(value = "SELECT DISTINCT category FROM product", nativeQuery = true)
   	List<String> listAllCategory();
    /* Tien Loc */
    @Transactional
	void deleteById(int id);
	Product findProductById(int productId);
}
