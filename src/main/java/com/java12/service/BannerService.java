package com.java12.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.java12.model.Banner;
import com.java12.repositories.BannerRepository;

@Service
public class BannerService {
	@Autowired
	private BannerRepository bannerRepository;
	public String randomName() {
		int size =5;
        int lowerLimit = 97;  
        int upperLimit = 122; 
        Random random = new Random();  
        StringBuffer r = new StringBuffer(size); 
        for (int i = 0; i < size; i++) {  
            int nextRandomChar = lowerLimit 
                                 + (int)(random.nextFloat() 
                                         * (upperLimit - lowerLimit + 1)); 
            r.append((char)nextRandomChar); 
        } 
        return r.toString(); 
	}

	public void saveBanner(Banner banner) {
		bannerRepository.save(banner);
	}
	public void deleleteBanner() {
		
	}
	
	public Banner topleftBanner() {
		List<Banner> banners = bannerRepository.findAll();
		for (Banner banner : banners) {
			System.out.println(banner);
			if(banner.getPosition().equalsIgnoreCase("top-left")){
				return banner;				
			}
		}
		return null;
	}
	public Banner toprightBanner() {
		List<Banner> banners = bannerRepository.findAll();
		for (Banner banner : banners) {
			if(banner.getPosition().equalsIgnoreCase("top-right")){
				return banner;				
			}
		}
		return null;
	}
	public List<Banner> topmidBanner() {
		List<Banner> banners = bannerRepository.findAll();
		List<Banner> mid = new ArrayList<Banner>();
		for (Banner banner : banners) {
			if(banner.getPosition().equalsIgnoreCase("top-middle")){
				mid.add(banner);				
			}
		}
		return mid;
	}
}
