package com.java12.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.java12.model.Cart;
import com.java12.model.Images;
import com.java12.model.Product;
import com.java12.repositories.CartRepository;
import com.java12.repositories.ProductRepository;

@Service
@Transactional
public class CartService {
	
	@Autowired
	ProductRepository productRepository;
	@Autowired
	CartRepository cartRepository;
	
	public void add( Cart cart) {
		cartRepository.saveAndFlush( cart);
	}
	
//	public void showCart(Cart cart) {
//		
//	}
	public void EditCart(Cart cart) {
		if(cart != null) {
			cartRepository.save(cart);
		}
	}
	public void DeleteCart(long id) {
		
			cartRepository.deleteById(id);
	}
	
	public List<Cart> listItems() {
		
		return cartRepository.findAll();
	}
	
	public List<Cart> getCartByUserId(long userId) {
		List<Cart> result = new ArrayList();
		List<Cart> findall = cartRepository.findAll();
		for(Cart cart : findall) {
			if(userId == cart.getUserId())result.add(cart);
		}
		return result;
	}
	
	
	public Product findItem(int productId){
		return productRepository.findProductById(productId);
	}

	
}

