package com.java12.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.mail.internet.MimeMessage;

@Component
public class EmailService {
    @Autowired
    private JavaMailSender javaMailSender;

    @Async
    public void sendVerifyEmail(String email,String baseUrl, String userID){
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");
        String verifyUrl = baseUrl + "/verify-account/" + userID;
        String htmlMsg = "To confirm your email, please " + "<a href=\"" + verifyUrl + "\"> CLICK HERE </a>";

        try {
            helper.setText(htmlMsg, true); // Use this or above line.
            helper.setTo(email);
            helper.setSubject("Comfirmation Email");
        }catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
        javaMailSender.send(mimeMessage);
    }
}
