package com.java12.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java12.model.Images;
import com.java12.repositories.ImageRepository;

@Service
public class ImageService {
    @Autowired
    ImageRepository repo;
    
    public List<Images> listImages(){
        return repo.findAll();
    }
    
    public List<Images> list(Long id_album){
        return repo.findAllByAlbum(id_album);
    }
    
    public void save(Images image) {
    	repo.save(image);
     }
    public void add(Images image) {
    	repo.saveAndFlush(image);
    }
   
}
