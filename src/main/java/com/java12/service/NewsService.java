package com.java12.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java12.model.Album;
import com.java12.model.News;
import com.java12.model.User;
import com.java12.repositories.NewsRepository;

@Service
public class NewsService {
	@Autowired
	NewsRepository newsRepo;
		
	public List<News> getAllNews(){
		return newsRepo.findAll();
	}
	
	public News findNewsById(int id) {
		
		return newsRepo.findById(id);
	}
	
	public List<News> findHotNews(){
		List<News> lstnews =  newsRepo.findOrderByCreated();
		List<News> resultList = new ArrayList<News>();
		int n;
		if(lstnews.size()>=3) {
			n = 3;
		}
		else {
			n=lstnews.size();
		}
		for (int i=0;i<n;i++) {

			resultList.add(lstnews.get(i));
		}
		
		return resultList;
	}
	
	public List<News> findViewMoreNews(){
		List<News> lstnews =  newsRepo.findOrderByView();
		List<News> resultList = new ArrayList<News>();
		int n;
		if(lstnews.size()>=8) {
			n = 8;
		}
		else {
			n=lstnews.size();
		}
		
		for (int i=0;i<n;i++) {

			resultList.add(lstnews.get(i));
		}
		
		return resultList;
	}
	
	/* Quang Dai - Add News */
	public void addNew(News news) {
    	newsRepo.save(news);
    }
	public List<String> listAllCategory(){
		return newsRepo.listAllCategory();
	}
	public List<News> getNewsByCategory(String category){
		return newsRepo.findByCategory(category)!=null?newsRepo.findByCategory(category):null;
	}
	public void deleteNews(int id) {
    	newsRepo.deleteById(id);
    } 
	public News getNews(int id) {
    	return newsRepo.findById(id);
    }
	

}
