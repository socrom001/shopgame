package com.java12.service;

import java.io.File;
import java.util.List;
import java.util.Optional;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.java12.model.Newsletter;
import com.java12.repositories.NewsletterRepository;
 
@Component
@Service
@Transactional
public class NewsletterService {
 
    @Autowired
    private NewsletterRepository repo;
     
    @Autowired
    private Environment env;
    
    public List<Newsletter> listAll() {
        return repo.findAll();
    }
     
    public void save(Newsletter newsletter) {
        repo.save(newsletter);
    }
     
    public Optional <Newsletter> getByEmail(String email) {
        return repo.findByEmail(email);
    }
     
    public void delete(long id) {
        repo.deleteById(id);
    }
    
    @Autowired
    private JavaMailSender javaMailSender;
    
    @Async
    public void sendNews(String subject, String content, String filepath, String filename) throws MessagingException{
    	
    	MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "utf-8");
        List<Newsletter> newsletterList = listAll();
        if (newsletterList.size()==0) {
        	return;
        }
        
        String email = "";
        for (Newsletter i : newsletterList) {
        	if (email == "")
        		email = i.getEmail();
        	else email = email + " , " + i.getEmail();
        }
        System.out.println("llllllll??:"+ email);
        try {
        	helper.setText(content, true); 
            helper.setTo(InternetAddress.parse(email));
            helper.setSubject(subject);
            FileSystemResource file = new FileSystemResource(new File(filepath + filename));
            helper.addAttachment(filename, file);
        }catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
        javaMailSender.send(mimeMessage);
    }
    
    @Async
    public void sendGreeting(String bseUrl, String email){
    	System.out.println("greeting" + email);
    	MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");
        String delUrl = bseUrl + "/newsletter_delete?email=" + email + "";
        System.out.println("url: " + delUrl);
        String htmlMsg = "Hi, email " + email + " is now subcribed to TBYT news service. "
        		+ "To unsubcribe email service, please " + "<a href=\"" + delUrl + "\"> CLICK HERE </a>";
        try {
        	System.out.println("greeting2" + email);
        	helper.setText(htmlMsg, true); 
            helper.setTo(InternetAddress.parse(email));
            helper.setSubject("Greeting From TBYT.vn");
        }catch(Exception ex) {
        	System.out.println("greetinger");
            System.out.println(ex.getMessage());
        }
        javaMailSender.send(mimeMessage);
        System.out.println("greeting2" + email);
    }

}