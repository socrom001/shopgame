package com.java12.service;

import com.java12.model.Album;
import com.java12.model.Product;
import com.java12.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {
    @Autowired
    ProductRepository productRepository;

    public List<Product> findByName(String name){
//    	System.out.print(name);
        return productRepository.findByNameContaining(name);
    }
    
    public Product findProductById(int id) {
    	return productRepository.findById(id);
    }
    
    public List<Product> listAll() {
        return productRepository.findAll();
    }
    
    public List<Product> findNewProduct(){
		List<Product> lstpro =  productRepository.findOrderById();
		List<Product> resultListPro = new ArrayList<Product>();
		int n;
		if(lstpro.size()>=4) {
			n = 4;
		}
		else {
			n=lstpro.size();
		}
		for (int i=0;i<n;i++) {

			resultListPro.add(lstpro.get(i));
		}
		
		return resultListPro;
	}
    
    public List<Product> getProductByCategory(String category){
    	return productRepository.findByCategory(category)!=null?productRepository.findByCategory(category):null;
	}
    public List<String> listAllCategory(){
		return productRepository.listAllCategory();
	}
    /* Tien Loc */
    public void addProduct(Product product) {
    	productRepository.save(product);
    }

	public void deleteProduct(int id) {
    	productRepository.deleteById(id);
	}
	/* LiemNT */
	public Product getProduct(Integer id) {
    	return productRepository.findById(id).get();
    }
}
