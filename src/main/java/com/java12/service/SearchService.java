package com.java12.service;

import com.java12.model.Product;
import com.java12.repositories.SearchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class SearchService {

    @Autowired
    public SearchRepository searchRepository;

    public Page<Product> findName(String keywork, Integer page){
        Pageable pageable = PageRequest.of(page-1,8);
        return searchRepository.findByNameContaining(keywork, pageable);
    }
}
